package main

import (
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"

	"gitlab.com/denis.dmitriev/gitlab-mr-bot.git/gitlab"
	"gitlab.com/denis.dmitriev/gitlab-mr-bot.git/mr"
)

var projectId uint64
var apiToken string
var action string
var mrId uint64

func initApp() {
	flag.Uint64Var(&projectId, "project-id", 0, "GitLab project ID")
	flag.StringVar(&apiToken, "token", "", "GitLab API token")
	flag.StringVar(&action, "do", "list", "Action to perform: [do, merge]")
	flag.Uint64Var(&mrId, "mr-iid", 0, "Merge Request IID")
	flag.Parse()

	if projectId == 0 {
		pidStr, ok := os.LookupEnv("VAR_PROJECT_ID")
		if ok {
			pid, okk := strconv.ParseUint(pidStr, 10, 64)
			if okk != nil {
				fmt.Println("VAR_PROJECT_ID must be valid uint64")
				os.Exit(1)
			}
			projectId = pid
		}
	}

	if len(apiToken) == 0 {
		apiToken = os.Getenv("VAR_TOKEN")
	}

	if projectId == 0 || len(apiToken) == 0 {
		fmt.Println("---")
		fmt.Println("Both variables VAR_TOKEN and VAR_PROJECT_ID must be defined")
		os.Exit(1)
	}
}

func doMerge() {
	if mrId == 0 {
		fmt.Println("Merge request ID must be specified. Use -mr-iid=... command line parameter")
		os.Exit(1)
	}
	api := gitlab.Api{Token: apiToken}
	api.MergeMr(projectId, mrId)
}

func doList() {
	selfPath, err := os.Executable()
	if err != nil {
		panic(err)
	}

	var mrs = mr.GetList(apiToken, projectId)

	menu := strings.Builder{}
	mergedCount := 0
	for _, mr := range mrs {
		fmt.Fprintf(&menu, "%s %s\n", getMrIcons(mr), mr.Title)
		if mr.IsReadyForMerge() {
			mergedCount++
			fmt.Fprintf(&menu, "-- Merge|shell=\"%s\" terminal=false "+
				"param1=-do=merge "+
				"param2=-project-id=%d "+
				"param3=-mr-iid=%d "+
				"param4=-token=%s "+
				"refresh=true\n",
				selfPath, projectId, mr.Iid, apiToken)
		}
		fmt.Fprintf(&menu, "-- Open|shell=\"open\" param1=%s\n", mr.Url)
	}

	// Write menu to console
	if mergedCount > 0 {
		fmt.Print("🟢")
	} else {
		fmt.Print("🔴")
	}
	fmt.Println("GL")
	fmt.Println("---")
	fmt.Print(menu.String())
}

func getMrIcons(mr mr.Info) string {
	r := ""
	if mr.IsReadyForMerge() {
		r += "🟢"
	} else {
		r += "🔴"
	}
	if mr.IsDraft {
		r += "📝"
	} else {
		r += "⚪"
	}
	if mr.HasConflicts {
		r += "⚠️"
	} else {
		r += "⚪"
	}
	if mr.UnresolvedThreads > 0 {
		r += "💬"
	} else {
		r += "⚪"
	}
	if mr.IsApproved {
		r += "👍"
	} else {
		r += "⚪"
	}

	return r
}

func main() {
	initApp()

	// gitlab.DumpJson = true
	if strings.ToLower(action) == "merge" {
		doMerge()
	}

	doList()
}
