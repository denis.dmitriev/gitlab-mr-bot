package mr

import (
	"sync"

	"gitlab.com/denis.dmitriev/gitlab-mr-bot.git/gitlab"
)

type user struct {
	id   uint64
	name string
}

type Info struct {
	ApprovedBy        []user
	HasConflicts      bool
	Id                uint64
	Iid               uint64
	IsApproved        bool
	IsDraft           bool
	Title             string
	UnresolvedThreads uint16
	Url               string
}

func (mr *Info) IsReadyForMerge() bool {
	return mr.IsApproved &&
		!mr.IsDraft &&
		!mr.HasConflicts &&
		mr.UnresolvedThreads == 0
}

// Returns the list of pending merge request info for given project
func GetList(token string, projectId uint64) []Info {
	api := gitlab.Api{Token: token}
	mrs, err := api.GetMrList(projectId)
	if err != nil {
		return nil
	}

	result := make([]Info, len(mrs))
	wg := sync.WaitGroup{}

	for i, mr := range mrs {
		result[i] = Info{
			HasConflicts: mr.HasConflicts,
			Id:           mr.Id,
			Iid:          mr.Iid,
			IsDraft:      mr.IsDraft,
			Title:        mr.Title,
			Url:          mr.WebUrl,
		}

		// Fetch and update MR approvers asynchronously
		wg.Add(1)
		go getApprovers(&api, projectId, mr.Iid, &result[i], &wg)
		// Fetch and count number of unresolved discussion threas
		if mr.UserNotesCount > 0 {
			wg.Add(1)
			go getComments(&api, projectId, mr.Iid, &result[i], &wg)
		}
	}
	wg.Wait()

	return result
}

func getApprovers(api *gitlab.Api, projectId, mrId uint64, mr *Info, wg *sync.WaitGroup) {
	defer wg.Done()

	ad, _ := api.GetMrApprovals(projectId, mrId)
	if len(ad.ApprovedBy) > 0 {
		mr.IsApproved = true
		mr.ApprovedBy = make([]user, len(ad.ApprovedBy))
		for j, a := range ad.ApprovedBy {
			mr.ApprovedBy[j] = user{a.User.Id, a.User.Name}
		}
	} else {
		mr.IsApproved = false
		mr.ApprovedBy = []user{}
	}
}

func getComments(api *gitlab.Api, projectId, mrId uint64, mr *Info, wg *sync.WaitGroup) {
	defer wg.Done()

	dd, _ := api.GetMrDisussions(projectId, mrId)
	var r uint16
	for _, d := range dd {
		for _, n := range d.Notes {
			if n.Type == "DiffNote" && n.Resolvable && !n.Resolved {
				r++
			}
		}
	}
	mr.UnresolvedThreads = r
}
