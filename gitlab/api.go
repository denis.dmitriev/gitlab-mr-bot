package gitlab

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httputil"
	"strings"
)

const apiPrefix = "https://gitlab.com/api/v4"

var DumpJson = false

type MrData struct {
	Id             uint64 `json:"id"`
	Iid            uint64 `json:"iid"`
	IsDraft        bool   `json:"draft"`
	Title          string `json:"title"`
	HasConflicts   bool   `json:"has_conflicts"`
	UserNotesCount uint32 `json:"user_notes_count"`
	WebUrl         string `json:"web_url"`
}

type ApprovalData struct {
	Id         uint64 `json:"id"`
	Iid        uint64 `json:"iid"`
	ApprovedBy []struct {
		User struct {
			Id   uint64 `json:"id"`
			Name string `json:"name"`
		} `json:"user"`
	} `json:"approved_by"`
}

type DiscussionsData struct {
	Id    string `json:"id"`
	Notes []struct {
		Id         uint64 `json:"id"`
		Type       string `json:"type"`
		Resolvable bool   `json:"resolvable"`
		Resolved   bool   `json:"resolved"`
	} `json:"notes"`
}

type requestError struct {
	code   int
	status string
}

type Api struct {
	Token string
}

func (m *requestError) Error() string {
	return m.status
}

func (a *Api) GetMrList(projectId uint64) ([]MrData, error) {
	url := fmt.Sprintf("%s/projects/%d/merge_requests?state=opened", apiPrefix, projectId)

	var res []MrData
	for {
		var err error
		var mrs []MrData

		mrs, url, err = request[[]MrData](a, "GET", url)
		if err != nil {
			return []MrData{}, err
		}
		res = append(res, mrs...)
		if url == "" {
			break
		}
	}

	return res, nil
}

func (a *Api) GetMrApprovals(projectId, mrId uint64) (ApprovalData, error) {
	url := fmt.Sprintf("%s/projects/%d/merge_requests/%d/approvals", apiPrefix, projectId, mrId)

	var ad, _, err = request[ApprovalData](a, "GET", url)
	if err != nil {
		return ApprovalData{}, err
	}

	return ad, nil
}

func (a *Api) GetMrDisussions(projectId, mrIid uint64) ([]DiscussionsData, error) {
	url := fmt.Sprintf("%s/projects/%d/merge_requests/%d/discussions", apiPrefix, projectId, mrIid)

	var res []DiscussionsData
	for {
		var err error
		var dd []DiscussionsData

		dd, url, err = request[[]DiscussionsData](a, "GET", url)
		if err != nil {
			return []DiscussionsData{}, err
		}
		res = append(res, dd...)
		if url == "" {
			break
		}
	}

	return res, nil
}

func (a *Api) MergeMr(projectId, mrIid uint64) (MrData, error) {
	url := fmt.Sprintf("%s/projects/%d/merge_requests/%d/merge", apiPrefix, projectId, mrIid)

	r, _, err := request[MrData](a, "PUT", url)
	if err != nil {
		return MrData{}, err
	}

	return r, nil
}

func request[T any](a *Api, operation, url string) (t T, nextPageUrl string, error error) {
	req, _ := http.NewRequest(operation, url, nil)
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", a.Token))
	var dflt T

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return dflt, "", err
	}
	if resp.StatusCode != http.StatusOK {
		return dflt, "", &requestError{resp.StatusCode, resp.Status}
	}
	defer resp.Body.Close()
	url, _ = getNextPageUrl(resp)

	bytesBody, _ := io.ReadAll(resp.Body)

	if DumpJson {
		b, _ := httputil.DumpRequest(req, false)
		fmt.Println(string(b))
		fmt.Println()
		b, _ = httputil.DumpResponse(resp, false)
		fmt.Println()
		fmt.Println(string(b))
		dumpJson(bytesBody)
	}
	var r T

	if err := json.NewDecoder(bytes.NewReader(bytesBody)).Decode(&r); err != nil {
		return dflt, "", err
	}

	return r, url, nil
}

// Returns next page URL by inspecting Link header in http.Response
func getNextPageUrl(r *http.Response) (url string, ok bool) {
	l := r.Header.Get("Link")
	if len(l) == 0 {
		return "", false
	}
	lm := parseLinkHeader(l)
	if url, ok := lm["next"]; ok {
		return url, true
	}
	return "", false
}

// Parses 'Link' header value and returns map where:
//   key:   link relation, e.g. first, last, prev, next
//   value: URL
// Map contains only the keys present in header value
func parseLinkHeader(v string) map[string]string {
	r := make(map[string]string)

	parts := strings.Split(v, ",")
	for _, v := range parts {
		pair := strings.Split(v, ";") // URL and its relation
		if len(pair) != 2 {
			panic(fmt.Sprintf("Unexpected Link header received: %s", v))
		}
		p := strings.Split(pair[1], "=") // rel="<first|last|next|prev>"
		rel := strings.Trim(p[1], " \"")
		url := strings.Trim(pair[0], " <>")
		r[rel] = url
	}
	return r
}

func dumpJson(b []byte) {
	prettyJson := bytes.Buffer{}
	json.Indent(&prettyJson, b, "", " . ")
	fmt.Println(prettyJson.String())
}
